import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { initGA, pageViewGA } from './components/Trackers';
import ga from './trackers.config.js';

import Navbar from './components/Navbar';
import Home from './components/Home';
import Services from './components/Services';
import Separator from './components/Separator';
import WorkPreview from './components/WorkPreview';
import Sell from './components/Sell';
import Devis from './components/Devis';
import Brands from './components/Brands';
import Contact from './components/Contact';
import Footer from './components/Footer';
import NotFound from './components/NotFound';


class App extends Component{

  componentDidMount(){
    initGA(ga.GA_UA_ID);
    pageViewGA();
  }

  render(){
    return (
      <div id="website" className="">
        <Router>

          <Navbar/>
          <Switch>
            <Route path="/" exact component={props =>
              <div>
                <Home/>
                <Services/>
                <Separator/>
                <WorkPreview/>
                <Sell/>
                <Devis/>
                <Brands/>
              </div>
            }/>

            <Route path="/home" component={props =>
              <div>
                <Home/>
                <Services/>
                <Separator/>
                <WorkPreview/>
                <Sell/>
                <Devis/>
                <Brands/>
              </div>
            }/>

            <Route path='/devdeas' component={() => {
              window.location.href = 'https://devdeas.enzoavagliano.fr';
              return null;
            }}/>

            <Route path='/blog' component={() => {
              window.location.href = 'https://blog.enzoavagliano.fr';
              return null;
            }}/>

            <Route path='/blinxon' component={() => {
              window.location.href = 'https://blinxon.enzoavagliano.fr';
              return null;
            }}/>

            <Route path="/contact" component={props =>
              <div>
                <Contact/>
              </div>
            }/>

            <Route component={props =>
              <div>
                <NotFound/>
              </div>
            }/>
          </Switch>
          <Footer/>
        </Router>
      </div>
    )
  }
}

export default App;
