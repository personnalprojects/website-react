import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import Navbar from './components/Navbar';

// General Ap tests
test('Render app', () => {
  render(<App/>);
  const appDOM = document.getElementById('website');
  expect(appDOM).toBeInTheDocument();
});

test('Navbar component rendered', () => {
  render(<App/>);
  const appDOM = document.querySelector('#navbar');
  expect(appDOM).toBeInTheDocument();
});
