import React from 'react';
import { withNamespaces } from 'react-i18next';
import { EventGA } from './Trackers';
import archee from '../img/brands/archee.png';
import visiopm from '../img/brands/visiopm.png';
import '../sass/brands.scss';

function Brands({ t }){
  return(
    <div id="brands" className="d-flex flex-column align-items-center p-4">
      <div className="d-flex flex-column align-items-end">
        <h1 className="title text-center">{t('brands.title')}</h1>
        {/* <p className="text-center subtitle m-0 ml-lg-2">{t('brands.subtitle')}</p> */}
      </div>
      <div className="h-100 d-flex flex-column flex-lg-row justify-content-center align-items-center">
        <a target="_blank" rel="noopener noreferrer" onClick={() => EventGA("brands_section", "User clicked on a link to go to brand website", "brands_link_redirect")} href="https://archee.fr/">
        <img src={archee} alt="Logo Archee"/>
        </a>
        <a target="_blank" rel="noopener noreferrer" onClick={() => EventGA("brands_section", "User clicked on a link to go to brand website", "brands_link_redirect")} href="http://www.visiopm.com/">
          <img src={visiopm} alt="Logo VisioPM"/>
        </a>
        <a className="ionos-partner" onClick={() => EventGA("brands_section", "User clicked on a link to go to brand website", "brands_link_redirect")} href="https://partnernetwork.ionos.fr/partner/enzo.avagliano?origin=PartnerBadge" rel="noopener noreferrer" target="_blank">
          <img style={{maxWidth:"100%"}} src="https://images-2.partnerportal.ionos.fr/items/9689055e-f69f-4945-b4f7-6603ff63ee66/profiles/799dba23-b582-4aef-b592-4bf4e27d5229/badges/normal_blue" alt="IONOS - Partenaire officiel"/>
        </a>
      </div>
    </div>
  )
}

export default withNamespaces()(Brands);
