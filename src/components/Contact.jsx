import React, { useState } from 'react';
import { withNamespaces } from 'react-i18next';
import { EventGA } from './Trackers';
import emailjs from 'emailjs-com';
import * as FIcons from "react-icons/fa";

import '../sass/contact.scss';
const mail_param = require('../mail.config.js');

function Contact({ t }){
  const [name, setName] = useState("");
  const [mail, setMail] = useState("");
  const [object, setObject] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    let finalMail = {
      "name":name,
      "mail":mail,
      "object":object,
      "date":new Date().toLocaleDateString('fr-FR'),
      "message":message
    }

    emailjs.init(mail_param.USER_ID);
    emailjs.send(mail_param.SERVICE_ID, mail_param.TEMPLATE_ID_CONTACT, finalMail)
    .then(function(response) {
       console.log('SUCCESS!', response.status, response.text);
       document.getElementById('feedback').style.color = "#2ecc71";
       document.getElementById('feedback').innerHTML = t('contact.form.success');
       EventGA("form_submit", "User sent a contact request", "contact_link");
       document.getElementById('contact-form').reset();
       setTimeout(function(){
         document.getElementById('feedback').innerHTML = "";
       },5000);
    }, function(error) {
       console.log('FAILED...', error);
       document.getElementById('feedback').style.color = "#e64c3c";
       document.getElementById('feedback').innerHTML = t('contact.form.error');
       EventGA("form_submit", "User sent a contact request but the request failed", "contact_link");
       document.getElementById('contact-form').reset();
       setTimeout(function(){
         document.getElementById('feedback').innerHTML = "";
       },5000);
    });
  }

  return(
    <div id="contact" className="d-flex flex-column align-items-center p-4">
      <div className="d-flex flex-column align-items-end">
        <h1 className="title text-center">{t('contact.title')}</h1>
      </div>
      <div className="mt-5 mb-5 mb-lg-0 mt-lg-0 col-10 p-0 d-flex flex-column flex-lg-row justify-content-center align-items-center">
        <div className="col-12 h-100 col-lg-3 d-flex flex-column justify-content-around align-items-center align-items-lg-start">
          <div className="box-contact text-center text-lg-left">
            <p className="box-info-title p-0 m-0"><FIcons.FaPhone/> <span className="pl-2">{t('contact.side-band.phone-title')}</span></p>
            <p className="box-info">{t('contact.side-band.phone')}</p>
          </div>
          <div className="box-contact text-center text-lg-left">
            <p className="box-info-title p-0 m-0"><FIcons.FaEnvelope/> <span className="pl-2">{t('contact.side-band.mail-title')}</span></p>
            <p className="box-info">{t('contact.side-band.mail')}</p>
          </div>
          <div className="box-contact text-center text-lg-left">
            <p className="box-info-title p-0 m-0"><FIcons.FaMapPin/> <span className="pl-2">{t('contact.side-band.adress-title')}</span></p>
            <p className="box-info">{t('contact.side-band.address')}<br/>{t('contact.side-band.address-2')}</p>
          </div>
          <div className="box-contact text-center text-lg-left">
            <p className="box-info-title p-0 m-0"><FIcons.FaLinkedin/> <span className="pl-2">{t('contact.side-band.linkedin-title')}</span></p>
            <p className="box-info">{t('contact.side-band.linkedin')}</p>
          </div>
        </div>
        <div className="col-12 col-lg-9">
          <form id="contact-form" className="form" onSubmit={handleSubmit}>
            <div className="form-row d-flex flex-row justify-content-between">
              <div className="form-group col-md-4 test">
                <input type="text" required className="form-control mt-4 required" id="requiredFileds" readOnly value={t('contact.form.required')}/>
                <input onChange={e => setName(e.target.value)} type="text" className="form-control mt-4" required id="inputName" placeholder={t('contact.form.name')}/>
                <input onChange={e => setMail(e.target.value)} type="email" className="form-control mt-4" required id="inputMail" placeholder={t('contact.form.mail')}/>
                <input onChange={e => setObject(e.target.value)} type="text" className="form-control mt-4" required id="inputObject" placeholder={t('contact.form.object')}/>
              </div>
              <div className="form-group col-md-8 pl-0 pl-md-4 pl-lg-4">
                <textarea onChange={e => setMessage(e.target.value)} id="inputMessage" className="form-control mt-4" required rows="9" placeholder={t('contact.form.message')}></textarea>
              </div>
            </div>
            <div className="col-12 p-0 text-center text-lg-right">
              <button type="submit" onClick={() => EventGA("form_submit", "User sent a contact request", "contact_link")} className="send-btn">{t('contact.form.submit')}</button>
              <p id="feedback"></p>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default withNamespaces()(Contact);
