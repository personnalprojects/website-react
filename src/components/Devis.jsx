import React, { useState } from 'react';
import { withNamespaces } from 'react-i18next';
import { EventGA } from './Trackers';
import '../sass/devis.scss';
import emailjs from 'emailjs-com';

const mail_param = require('../mail.config.js');

function Devis({ t }){

  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [description, setDescription] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    let devis = {
      "name":name,
      "mail":email,
      "type":type,
      "date":new Date().toLocaleDateString('fr-FR'),
      "description":description
    }

    emailjs.init(mail_param.USER_ID);
    emailjs.send(mail_param.SERVICE_ID, mail_param.TEMPLATE_ID_QUOTE, devis)
    .then(function(response) {
       console.log('SUCCESS!', response.status, response.text);
       document.getElementById('feedback').style.color = "#2ecc71";
       document.getElementById('feedback').innerHTML = t('devis.form.success');
       EventGA("form_submit", "User sent a quote request", "contact_link");
       document.getElementById('quote-form').reset();
       setTimeout(function(){
         document.getElementById('feedback').innerHTML = "";
       },5000);
    }, function(error) {
       console.log('FAILED...', error);
       document.getElementById('feedback').style.color = "#e64c3c";
       document.getElementById('feedback').innerHTML = t('devis.form.error');
       EventGA("form_submit_fail", "User sent a contact request but the request failed", "contact_link");
       document.getElementById('quote-form').reset();
       setTimeout(function(){
         document.getElementById('feedback').innerHTML = "";
       },5000);
    });
  }

  return(
    <div id="devis" className="d-flex flex-column align-items-center p-4">
      <div className="d-flex flex-column align-items-end">
        <h1 className="title text-center">{t('devis.title')}</h1>
        <p className="m-0 ml-lg-2">{t('devis.subtitle')}</p>
      </div>
      <div className="d-flex flex-column flex-lg-row col-12 col-lg-9 pt-5">
        <div className="col-12 col-lg-6 d-flex flex-column justify-content-center h-100">
          <form id="quote-form" onSubmit={handleSubmit}>
            <div className="form-group col-md-12">
              <label htmlFor="inputEmail">{t('devis.form.mail_field')}</label>
              <input required onChange={e => setEmail(e.target.value)} type="email" className="form-control" id="inputEmail" placeholder={t('devis.form.mail_field_placeholder')}/>
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="inputName">{t('devis.form.name_field')}</label>
              <input onChange={e => setName(e.target.value)} type="text" className="form-control" id="inputName" placeholder={t('devis.form.name_field_placeholder')}/>
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="inputType">{t('devis.form.project_field')}</label>
              <select onChange={e => setType(e.target.value)} id="inputState" className="form-control">
                <option defaultValue={t('devis.form.options.unspecified')}>{t('devis.form.project_field_placeholder')}</option>
                <option value={t('devis.form.options.one')}>{t('devis.form.options.one')}</option>
                <option value={t('devis.form.options.two')}>{t('devis.form.options.two')}</option>
                <option value={t('devis.form.options.tree')}>{t('devis.form.options.tree')}</option>
                <option value={t('devis.form.options.four')}>{t('devis.form.options.four')}</option>
                <option value={t('devis.form.options.other')}>{t('devis.form.options.other')}</option>
              </select>
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="inputDescription">{t('devis.form.description_field')}</label>
              <textarea onChange={e => setDescription(e.target.value)} className="form-control" id="inputDescription" rows="5" placeholder={t('devis.form.description_field_placeholder')}></textarea>
            </div>
            <div className="form-group col-md-12 d-flex flex-column align-items-center">
              <button type="submit" className="btn btn-primary">{t('devis.form.send')}</button>
              <p id="feedback"></p>
            </div>
          </form>
        </div>
        <div className="devis-description mt-5 mt-lg-0 col-12 col-lg-6 d-flex flex-column justify-content-center">
          <h3 className="text-center text-lg-left">{t('devis.content_title')}</h3>
          <p className="text-center text-lg-left">{t('devis.content_subtitle')}</p>
          <p className="text-center text-lg-left">{t('devis.content_description')}</p>
        </div>
      </div>
    </div>
  )
}

export default withNamespaces()(Devis);
