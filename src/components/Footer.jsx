import React, { Component } from 'react';
import '../sass/home.scss';

class Footer extends Component{

  componentDidMount() {
    var year = new Date().getFullYear();
    document.getElementById('year').innerHTML = year;
  }
  render(){
    return(
      <div id="footer" className="d-flex flex-row align-items-center justify-content-center m-0 p-0">
        <p className="p-0 m-0">&#169; Enzo Avagliano <span className="year" id="year"></span></p>
      </div>
    )
  }
}

export default Footer;
