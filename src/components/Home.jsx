import React from 'react';
import { withNamespaces } from 'react-i18next';
import { EventGA } from './Trackers';

import logo from '../img/logos/ea.svg';
import '../sass/home.scss';

function Home({ t }){
  return(
    <div id="home" className="d-flex flex-column align-items-center justify-content-center">
      <div className="title-container d-flex flex-column flex-lg-row">
        <div className="d-flex flex-row align-items-center justify-content-center text-lg-left">
          <img className="main-logo" src={logo} alt={t('images.logo')}/>
        </div>
        <div className="d-flex flex-column justify-content-center text-center align-items-center align-items-lg-start">
          <p className="main-title m-0 p-0">Enzo Avagliano</p>
          <p className="main-subtitle m-0 p-0">{t('job.description')}</p>
        </div>
      </div>
      <div className="catch-container w-75 d-flex flex-column justify-content-center align-items-center text-center">
        <p className="catch-line">{t('sentences.catch-line')}</p>
        <p className="question-line">{t('sentences.question-line')}</p>
        <a onClick={() => EventGA("link_click", "User clicked home quote button", "quote_section_redirect_button")} className="quote-link" href="#devis">{t('sentences.quote-link')}</a>
      </div>
    </div>
  )
}

export default withNamespaces()(Home);
