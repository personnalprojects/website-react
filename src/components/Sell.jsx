import React from 'react';
import Packs from './sub/Packs';
import '../sass/sell.scss';

function Sell({t}){
  return(
    <div id="packs" className="d-flex flex-column align-items-center pt-4">
      <h1 className="title">Mes offres</h1>
      <div className="d-flex flex-wrap justify-content-around align-items-center p-2 mt-4">
        <Packs cat="Starter" price="1200" desc="Développement de votre site vitrine avec l'outil Wordpress. Le moyen le plus simple de vous faire connaître sur le web." tips={["Utilisation de votre charte graphique", "Votre site responsive", "Votre nom de domaine + votre hébergement", "Votre contenu et vos images", "Référencement optimal"]}/>
        <Packs cat="Custom" price="1500" best desc="Développement de votre site vitrine sur-mesure (codé à la main) vous offrant une plus ample personnalisation." tips={["Utilisation de votre charte graphique", "Votre site responsive", "Votre nom de domaine + votre hébergement", "Votre contenu et vos images", "Référencement optimal", "Espace administrateur (en option)", "Statistiques en temps réel (en option)", "Maintenance incluse pendant 6 mois"]}/>
        <Packs cat="E-commerce" price="2500" desc="Votre site E-commerce développé sous Wordpress. Attirez une nouvelle clientèle avec votre catalogue en ligne ! " tips={["Utilisation de votre charte graphique", "Votre site responsive", "Votre nom de domaine + votre hébergement", "Votre contenu et vos images", "Référencement optimal", "Paiements en ligne", "Espace administrateur", "Statistiques en temps réel", "Maintenance incluse pendant 6 mois"]}/>
        <Packs cat="Maintenance" price="200" annual desc="Le pack maintenance vous offre la possibilité de faire évoluer votre site dans le temps, sans avoir à vous soucier des détails." tips={["Mise a jour de contenu", "Modifications de visuels (légères)", "Ajouts d'options", "Gestion nom de domaine & hébergement"]}/>
      </div>
      <small className="disclaimer mb-2">* Les prix affichés sont des prix de référence et sont suceptibles de varier d'un devis à un autre. - ** Le pack maintenance ne nécéssite pas d'être déjà client.</small>
    </div>
  )
}

export default Sell;
