import React from 'react';
import { withNamespaces } from 'react-i18next';
import '../sass/separator.scss';

function Separator({ t }){
  return(
    <div id="separator" className="d-flex flex-row justify-content-center align-items-center">
      <blockquote className="blockquote m-0 p-0">
        <p className="m-0 quote">&laquo; {t('separator.quote')} &raquo;</p>
      </blockquote>
    </div>
  )
}

export default withNamespaces()(Separator);
