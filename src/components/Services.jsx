import React from 'react';
import { withNamespaces } from 'react-i18next';
import * as Icons from 'react-bootstrap-icons';
import mockup from '../img/mockups/mockup_self.png';
import '../sass/services.scss';

function Services({ t }){

  const $ = require('jquery');
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });


  return(
    <div id="services" className="d-flex flex-column align-items-center pt-5">
      <h1 className="service-title">{t('services.title')}</h1>
      <div className="mt-3 mb-5 col-12 col-lg-10 d-flex flex-wrap justify-content-center">
        <div className="col-11 col-lg-6 d-flex flex-column align-items-center justify-content-center">
          <p className="catch-phrase text-center">{t('services.catch-phrase')}</p>
          <div className="service-box mt-3 w-100 d-flex flex-column flex-lg-row" >
            <div className="icon-box d-flex flex-row justify-content-center align-items-center">
              <p className="p-0 m-0"><Icons.Scissors color="#9146FF"/></p>
            </div>
            <div className="text-box d-flex flex-row justify-content-center justify-content-lg-start align-items-center">
              <p className="p-0 m-0 text-center" title={t('services.boxes.tooltip.tailor-made')}>{t('services.boxes.tailor-made')}</p>
            </div>
          </div>
          <div className="service-box mt-3 w-100 d-flex flex-column flex-lg-row">
            <div className="icon-box d-flex flex-row justify-content-center align-items-center">
              <p className="p-0 m-0"><Icons.Signpost2 color="#9146FF"/></p>
            </div>
            <div className="text-box d-flex flex-row justify-content-center justify-content-lg-start align-items-center">
              <p className="p-0 m-0 text-center text-lg-left" title={t('services.boxes.tooltip.support')}>{t('services.boxes.support')}</p>
            </div>
          </div>
          <div className="service-box mt-3 w-100 d-flex flex-column flex-lg-row">
            <div className="icon-box d-flex flex-row justify-content-center align-items-center">
              <p className="p-0 m-0"><Icons.GraphUp color="#9146FF"/></p>
            </div>
            <div className="text-box d-flex flex-row justify-content-center justify-content-lg-start align-items-center">
              <p className="p-0 m-0 text-center text-lg-left" title={t('services.boxes.tooltip.seo')}>{t('services.boxes.seo')}</p>
            </div>
          </div>
          <div className="service-box mt-3 w-100 d-flex flex-column flex-lg-row">
            <div className="icon-box d-flex flex-row justify-content-center align-items-center">
              <p className="p-0 m-0"><Icons.ArrowsFullscreen color="#9146FF"/></p>
            </div>
            <div className="text-box d-flex flex-row justify-content-center justify-content-lg-start align-items-center">
              <p className="p-0 m-0 text-center text-lg-left" title={t('services.boxes.tooltip.responsive')}>{t('services.boxes.responsive')}</p>
            </div>
          </div>
          <div className="service-box mt-3 w-100 d-flex flex-column flex-lg-row">
            <div className="icon-box d-flex flex-row justify-content-center align-items-center">
              <p className="p-0 m-0"><Icons.Gear color="#9146FF"/></p>
            </div>
            <div className="text-box d-flex flex-row justify-content-center justify-content-lg-start align-items-center">
              <p className="p-0 m-0 text-center text-lg-left" title={t('services.boxes.tooltip.maintenance')}>{t('services.boxes.maintenance')}</p>
            </div>
          </div>
          <div className="service-box mt-3 w-100 d-flex flex-column flex-lg-row">
            <div className="icon-box d-flex flex-row justify-content-center align-items-center">
              <p className="p-0 m-0"><Icons.Eyeglasses color="#9146FF"/></p>
            </div>
            <div className="text-box d-flex flex-row justify-content-center justify-content-lg-start align-items-center">
              <p className="p-0 m-0 text-center text-lg-left" title={t('services.boxes.tooltip.transparency')}>{t('services.boxes.transparency')}</p>
            </div>
          </div>
        </div>
        <div className="col-8 col-lg-6 text-center">
          <img className="mockup" src={mockup} alt={t('images.mockup')}/>
        </div>
      </div>
    </div>
  )
}

export default withNamespaces()(Services);
