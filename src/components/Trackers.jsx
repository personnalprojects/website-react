import ReactGA from 'react-ga';

export const initGA = (ua_tracking_id) => {
  ReactGA.initialize(ua_tracking_id);
};

export const pageViewGA = () => {
  ReactGA.pageview(window.location.pathname + window.location.search);
};

export const EventGA = (category, description, label) => {
  ReactGA.event({
    category: category,
    action: description,
    label: label
  });
};
