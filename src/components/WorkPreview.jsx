import React from 'react';
import { withNamespaces } from 'react-i18next';
import WorkCard from './sub/WorkCard.jsx';
import '../sass/workpreview.scss';

function WorkPreview({ t }){
  return(
    <div id="workpreview" className="pt-5 mb-5 d-flex flex-column align-items-center">
      <p className="h1 workpreview-title">{t('work.work-title')}</p>
      <p className="h6 workpreview-subtitle">{t('work.work-subtitle')}</p>
      <div className="mt-5 d-flex flex-wrap justify-content-center col-12">
        <WorkCard title={t('work.workcards.website.name')} description={t('work.workcards.website.description')} image="https://media.discordapp.net/attachments/814250385190420483/846304233241968680/enzoavagliano.png?width=1328&height=653" url={t('work.workcards.website.url')} git_url={t('work.workcards.website.git_url')}/>
        <WorkCard title={t('work.workcards.devdeas.name')} description={t('work.workcards.devdeas.description')} image="https://cdn.discordapp.com/attachments/814250385190420483/846303472868655124/devdeas.png" url={t('work.workcards.devdeas.url')} git_url={t('work.workcards.devdeas.git_url')}/>
        <WorkCard title={t('work.workcards.visiopm.name')} description={t('work.workcards.visiopm.description')} image="https://media.discordapp.net/attachments/814250385190420483/850082761573924904/unknown.png?width=1330&height=653" url={t('work.workcards.visiopm.url')} git_url={t('work.workcards.visiopm.git_url')}/>
      </div>
    </div>
  )
}

export default withNamespaces()(WorkPreview);
