import React, { useState } from 'react';
import i18n from "i18next";
import { withNamespaces } from 'react-i18next';
import { EventGA } from '../Trackers';

import '../../sass/langSelector.scss';

function LangSelector(){
  const [lang, setLang] = useState('fr');

  const handleClickLang = () => {
    if (lang === "fr") {
      setLang("en");
      i18n.changeLanguage('en');
    } else {
      setLang("fr");
      i18n.changeLanguage('fr');
    }

    EventGA("language_change", "User changed website language", "language_selector")
  };

  return(
    <button className="nav-item lang-item" onClick={handleClickLang}>
      <span role="img" aria-label="Français">🇫🇷</span> / <span role="img" aria-label="English">🇬🇧</span>
    </button>
  )
}

export default withNamespaces()(LangSelector);
