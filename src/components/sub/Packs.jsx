import React from 'react';
import bestOffer from '../../img/logos/bestOffer.png';
import '../../sass/sub/packs.scss';

function Packs({t, cat, desc, tips, price, annual, best}){
  return(
    <div className="pack mx-3 my-2 p-2 d-flex flex-column justify-content-between">
      <div className="">
        <p className="pack-title text-center">{cat} {annual ? <sup>**</sup> : ""}</p>
        <div className="pack-body col-12">
          <p className="text-center mb-5 mb-lg-2">{desc ? desc : "No description"}</p>
          {
            tips ?
            tips.map((item, index) => {
              return(
                  <div key={index} className="pack-tip">
                    {index === 0 ? <hr className="m-0 p-0"/> : ""}
                    <p className="m-0 p-0 py-2">{item}</p>
                    <hr className="m-0 p-0"/>
                  </div>
              )
            })
            :
            ""
          }
          {/* <div className="pack-tip">
            <p className="m-0 p-0 py-2">Et plus encore</p>
            <hr className="m-0 p-0"/>
          </div> */}
        </div>
      </div>
      <div className="pack-infos mb-3">
        <div className="d-flex flex-row justify-content-between p-0 col-12 pl-3">
          <div className="col-8 p-0 pt-3">
            <small>A partir de</small>
            <p className="pack-price"><strong>{price ? price : "no_price"}€</strong> <sup>TTC</sup> {annual ? <span className="annual-tag">/an </span> : ""}<sup>*</sup></p>
          </div>
          <div className="col-4 p-0 pt-3 d-flex flex-row justify-content-end">
            {
              best ?
              <img src={bestOffer} className="recomanded-img" alt="Étiquette d'offre recomandée"/>
              :
              ""
            }
          </div>
        </div>
        <div className="text-center">
          <a href="#devis" className="pack-button p-2">Plus d'infos</a>
        </div>
      </div>
    </div>
  )
}

export default Packs;
