import React from 'react';
import { withNamespaces } from 'react-i18next';
import '../../sass/sub/workcard.scss';

function WorkCard({t, title, url, git_url, description, image }){
  return(
    <div className="workcard col-12 col-lg-3 d-flex flex-column justify-content-between m-3 p-0">
      <img className="workcard-image" src={image} alt={url}/>
      <div className="workcard-content p-2">
        <div className="workcard-text">
          <p className="workcard-title p-0 m-0 black">{title}</p>
          <p className="workcard-description black">{description}</p>
        </div>
        <hr/>
      </div>
      <div className="workcard-urls d-flex flex-column p-2">
        <a className="workcard-link" href={url} target="_blank" rel="noopener noreferrer">{t(`work.workcards.url_tag`)}</a>
        {git_url !== "" ? <a className="workcard-link" href={git_url} target="_blank" rel="noopener noreferrer">{t(`work.workcards.git_url_tag`)}</a> : <a className="workcard-link" href="/">{t(`work.workcards.git_url_tag_alternate`)}</a>}
      </div>
    </div>
  )
}

export default withNamespaces()(WorkCard);
